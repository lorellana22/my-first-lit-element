# my-first-lit-element

## Qué es LitElement

En una clase base para la creación de Custom Elements que tan solo pesa 6,6kb.

Facilita la creación de componentes nativos obteniendo varios beneficios de serie como:

- Data-binding
- Sistema de templates reactivo
- Constructable stylesheets
- Sincronización entre propiedades y atributos

Hace uso de lit-html para la creación de templates extensibles, eficientes y expresivos; es decir, LitElement usa lit-html como motor de plantillas.
Para poder hacer uso de ella vamos a ejecutar:

`npm install --save lit`

## Evolución de un Web Component hacia LitElement

Tomando un *web component nativo* de referencia, en este componente voy a importar las funciones html y render de lit-html. La primera sirve para no escribir directamente sobre la propiedad innerHTML y poder dividir mejor el template; y la segunda para mostrar el contenido de los templates.

De esta forma ahora extiendo de LitElement, lo que hace que el método render sea un método independiente que tiene que devolver un template escrito con lit-html. Además, permite definir los estilos fuera del HTML con la función auxiliar "css" y definir las propiedades del componente y el tipo de converter.



