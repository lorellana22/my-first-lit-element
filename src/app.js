import { css, html, LitElement } from "lit";

export class AppElement extends LitElement {
 

  static get styles() {
    //como esto es un objeto de JavaScript que devuelve esa función,
    //aqui podemos jugar: si queremos tener css compartido, podemos tener
    //una funcion que nos devuelva un css que queramos aplicar a todas partes
    //y aqui se devuelva la combinacion de eso + los estilos especificos del web component
    return css`
      :host {
        display: block;
        border: black solid 2px;
      }
      p {
        color: var(--color-poc, blue);
      }
      ::slotted(p) {
        color: red;
      }
    `;
  }
  static get properties() { //el valor inicial de las propiedades se debe 
    //poner en el connectedcallback
    return {
      hello: { 
        type: String,
        state: true
      }
    };
  }
  render() {
    // this.hello = "Lorena"; Esto cambia el estado interno del componente, y no se hace dentro del render porque perdemos la reactividad
    return html`
      <p part="hello">${this.hello}</p>
      <slot></slot>
      <input type="text" .value="${this.value}">

      <button @click="${this.clickMe}" ?disabled="${false}">Click!</button>
    `;
  }

  clickMe(e) {
    console.log(e);
    this.hello = "Evento";
    const message = new CustomEvent("poc:message", {
      //propiedades:
      bubbles: true,
      composed: true, //cnd queremos que otro elemento dentro del shadowDom se entere de este evento
      cancelable: true, //indica si se puede hacer un preventDefault de este evento o no
      detail: {
        msg: "hola desde el componente",
      },
    });
    this.dispatchEvent(message);
  }

  //las llamadas asíncronas se hacen dentro del connectedcallBack
}
customElements.define("app-element", AppElement); 

