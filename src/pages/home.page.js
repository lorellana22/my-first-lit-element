import { AppElement } from "../app";


export class HomePage extends HTMLElement {

constructor () {
    super();
}

connectedCallback() {
    this.innerHTML = `
    <h1>My first Lit Element</h1>
    <app-element hello="hola lorena">¿que se va a mostrar?</app-element>
    `;
    
}
}
customElements.define("home-page", HomePage)